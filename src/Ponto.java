public class Ponto {
    int idx;
    private double x;
    private double y;

    public Ponto(int idx, String line){
        String [] valores = line.split("\t");
        this.x= Double.parseDouble(valores[0]);
        this.y= Double.parseDouble(valores[1]);
    }

    public float getIndex(){
        return idx;
    }

    double getDistance(Ponto ponto){
        return Math.sqrt(Math.pow( (this.x-ponto.x), 2) + Math.pow( (this.y-ponto.y), 2));
    }
}
