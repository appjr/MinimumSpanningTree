import javax.xml.crypto.Data;
import java.util.Vector;

public class MST{


    public static void main(String [] strings) {
        Vector<String> lines = new FileServices().loadFile();
        if(strings[0]==null){
            printUso();
        } else if(strings[0].equalsIgnoreCase("PRIM")) {
            doPrim(lines);
        } else if(strings[0].equalsIgnoreCase("KRUSKAL")) {
            doKruskal(lines);
        }
    }

    private static void printUso() {
        System.out.println("USO: MST <opcao>");
        System.out.println("<opcao> = PRIM ou KRUSKAL");
    }

    private static void doKruskal(Vector<String> lines){
        new Kruskal(DataService.linesToInputKruskal(lines)).doKruskal();

        //Debug only
        //new Kruskal(Debug.getInputVector()).doKruskal();
    }

    private static void doPrim(Vector<String> lines) {
        String [][] input = DataService.linesToInputPrim(lines);
        new Prim(input).doPrim();

       //Debug only
        //new Prim(Debug.input).doPrim();
    }
}
