public class PontosDistancia  implements  Comparable<PontosDistancia>{
    private String ponto1;
    private String ponto2;
    private double distancia;

    public PontosDistancia(String ponto1, String ponto2, double distancia){
        this.setPonto1(ponto1);
        this.setPonto2(ponto2);
        this.setDistancia(distancia);
    }

    public String getPonto1() {
        return ponto1;
    }

    public void setPonto1(String ponto1) {
        this.ponto1 = ponto1;
    }

    public String getPonto2() {
        return ponto2;
    }

    public void setPonto2(String ponto2) {
        this.ponto2 = ponto2;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }


    @Override
    public int compareTo(PontosDistancia o) {
        int ret = 0;
        double res = this.getDistancia()-o.getDistancia();
        if(res<0){
            ret = -1;
        } else if(res>0){
            ret = 1;
        }
        return ret;
    }

    public String getPontosDistanciaDescription(){
        return "Vertice 1 _"+ getPonto1()+ " Vertice 2 _"+getPonto2()+" Valor "+getDistancia();
    }
}
