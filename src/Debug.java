import java.util.Vector;

public class Debug {

    public static String [][] input = {
            {"A", "B", "10"},
            {"A", "C", "20"},
            {"B", "D", "5"},
            {"B", "C", "30"},
            {"C", "E", "6"},
            {"C", "D", "16"},
            {"D", "E", "8"}
    };

    public static Vector<PontosDistancia> getInputVector(){
        Vector<PontosDistancia> toReturn = new Vector<PontosDistancia>();
        toReturn.add(new PontosDistancia("B","D",5));
        toReturn.add(new PontosDistancia("C","E",6));
        toReturn.add(new PontosDistancia("D","E",8));
        toReturn.add(new PontosDistancia("C","D",15));
        toReturn.add(new PontosDistancia("A","B",10));
        toReturn.add(new PontosDistancia("A","C",20));
        toReturn.add(new PontosDistancia("B","C",30));
        return toReturn;
    }
}
