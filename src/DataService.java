import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class DataService {
    static public String [] [] linesToInputPrim(Vector<String> lines){
        String [] [] toReturn = new String[lines.size()*(lines.size()-1)][3];
        Ponto [] pontos = new Ponto[lines.size()];
        for(int i=0;i<pontos.length;i++){
            pontos[i] = new Ponto(i, lines.get(i));
        }
        int nitem = 0;
        for(int nPontoRef=0;nPontoRef<pontos.length;nPontoRef++){
            Ponto ponto = pontos[nPontoRef];
            for(int pontoN=0;pontoN<pontos.length;pontoN++){
                if(nPontoRef!=pontoN){
                    double distance = ponto.getDistance(pontos[pontoN]);
                    toReturn[nitem++]=new String[]{""+nPontoRef, ""+pontoN,""+distance};
                }
            }
        }

        return toReturn;
    }

    static public Vector<PontosDistancia> linesToInputKruskal(Vector<String> lines){
        Vector<PontosDistancia> toReturn = new Vector<>();
        Ponto [] pontos = new Ponto[lines.size()];
        for(int i=0;i<pontos.length;i++){
            pontos[i] = new Ponto(i, lines.get(i));
        }
        int nitem = 0;
        for(int nPontoRef=0;nPontoRef<pontos.length;nPontoRef++){
            Ponto ponto = pontos[nPontoRef];
            for(int pontoN=0;pontoN<pontos.length;pontoN++){
                if(nPontoRef!=pontoN){
                    double distance = ponto.getDistance(pontos[pontoN]);
                    toReturn.add(new PontosDistancia(""+nPontoRef,""+pontoN,distance));
                }
            }
        }
        Collections.sort(toReturn, new PontosDistanciaComparator());
        return toReturn;
    }


    public static class PontosDistanciaComparator implements Comparator<PontosDistancia> {
        @Override
        public int compare(PontosDistancia pd1, PontosDistancia pd2) {
            return pd1.compareTo(pd2);
        }
    }

}
