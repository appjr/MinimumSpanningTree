public class ItemArvore {
    private String pontoA = null;
    private String pontoB = null;
    private double valor = 0;

    public ItemArvore(String a, String b, String valor){
        this.pontoA = a;
        this.pontoB = b;
        this.valor = Double.parseDouble(valor);
    }

    public String getPontoA() {
        return pontoA;
    }

    public void setPontoA(String pontoA) {
        this.pontoA = pontoA;
    }

    public String getPontoB() {
        return pontoB;
    }

    public void setPontoB(String pontoB) {
        this.pontoB = pontoB;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

}
