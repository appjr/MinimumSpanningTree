public class Ramo {
    public static double VALORMAX = 100000000;
    String vertice = null;
    double valor = VALORMAX; //-1 indica infinito
    String verticePai = null;
    boolean retirado = false;

    public Ramo(String vertice){
        this.vertice = vertice;
    }

    public String getVertice() {
        return vertice;
    }

    public void setVertice(String vertice) {
        this.vertice = vertice;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public boolean getRetirado() {
        return retirado;
    }

    public void setRetirado(boolean retirado) {
        this.retirado = retirado;
    }

    public String getVerticePai() {
        return verticePai;
    }

    public void setVerticePai(String verticePai) {
        this.verticePai = verticePai;
    }

    public String getRamoDescription(){
        return "Vertice _"+getVertice()+ " Pai _"+getVerticePai()+" Valor "+getValor();
    }

}
