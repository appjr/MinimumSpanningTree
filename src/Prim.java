import java.util.*;

public class Prim {
    
    ArrayList<ItemArvore> arvore = new ArrayList<ItemArvore>();
    Hashtable<String, Ramo> ramos = new Hashtable<>();
    Ramo ramoInicial = null;

    public Prim(String [] [] input){
        criaArvoreEntrada(input);
    }

    private void criaArvoreEntrada(String[][] input) {
        for(int i=0;i< input.length;i++){
            arvore.add(new ItemArvore(input[i][0],input[i][1],input[i][2]));
        }
    }

    public void doPrim(){
        iniciaRamos();
        processaRamos();
        printTodosRamos();
    }

    private void processaRamos() {
        ramoInicial.setVerticePai(null);
        ramoInicial.setRetirado(true);
        ramoInicial.setValor(0);
        Ramo ramo = ramoInicial;
        while(ramo!=null){
            for (int idx = 0; idx < arvore.size(); idx++) {
                ramo.setRetirado(true);
                ItemArvore itemArvore = arvore.get(idx);
                if (itemArvore.getPontoA().equals(ramo.getVertice()) || itemArvore.getPontoB().equals(ramo.getVertice())) {
                    Ramo ramoFilho = null;
                    if(ramo.getVertice().equals(itemArvore.getPontoA())) {
                        ramoFilho = ramos.get(itemArvore.getPontoB());
                    } else {
                        ramoFilho = ramos.get(itemArvore.getPontoA());
                    }
                    double valor = itemArvore.getValor();
                    if (!ramoFilho.getRetirado() && (ramoFilho.getValor()==Ramo.VALORMAX || ramoFilho.getValor() > valor)) {
                        ramoFilho.setValor(valor);
                        ramoFilho.setVerticePai(ramo.getVertice());
                    }
                }
            }
            ramo = getNextRamo();
        }
    }

    private Ramo getNextRamo(){
        Ramo ramoToReturn = null;
        Collection<Ramo> ramosCollection = ramos.values();
        Object[] ramosArray = ramos.values().toArray();
        for(int nRamo=0;nRamo<ramosArray.length;nRamo++) {
            Ramo ramo = (Ramo) ramosArray[nRamo];
            if(!ramo.getRetirado() && ramo.getValor()>=0){
                if(ramoToReturn == null || (ramoToReturn!=null && ramoToReturn.getValor()>ramo.getValor())){
                    ramoToReturn = ramo;
                }
            }
        }
        return ramoToReturn;
    }

    private void printTodosRamos(){
        Collection<Ramo> ramosCollection = ramos.values();
        Object[] ramosArray = ramos.values().toArray();
        for(int nRamo=0;nRamo<ramosArray.length;nRamo++) {
            System.out.println(((Ramo)ramosArray[nRamo]).getRamoDescription());
        }
    }

    private void iniciaRamos(){
        for(int i=0;i< arvore.size();i++){
            String pontoA = arvore.get(i).getPontoA();
            String pontoB = arvore.get(i).getPontoB();
            if(ramos.get(pontoA)==null){
                Ramo novoRamo = new Ramo(pontoA);
                if(ramos.size()==0){
                    novoRamo.setValor(0); //ponto de início
                    ramoInicial = novoRamo;
                }
                ramos.put(pontoA,novoRamo);
            }
            if(ramos.get(pontoB)==null){
                ramos.put(pontoB,new Ramo(pontoB));
            }
        }
    }


}
