import java.util.Collection;
import java.util.HashSet;
import java.util.Vector;

public class Kruskal {

    Vector<PontosDistancia> resultado = new Vector<PontosDistancia>();
    Vector<PontosDistancia> pontosDistancias;
    Vector<HashSet> aglomerados = new Vector<HashSet>();

    public Kruskal(Vector<PontosDistancia> pontosDistancias){
        this.pontosDistancias = pontosDistancias; //Já ordenado em ordem crescente de distância
    }

    public void doKruskal() {
        for(int idx = 0;idx<pontosDistancias.size();idx++){
            PontosDistancia pontosDistancia = pontosDistancias.get(idx);
            HashSet aglomerado1 = procuraEmAglomerado(pontosDistancia.getPonto1());
            HashSet aglomerado2 = procuraEmAglomerado(pontosDistancia.getPonto2());
            if(aglomerado1==null && aglomerado2!=null){
                aglomerado2.add(pontosDistancia.getPonto1());
                resultado.add(pontosDistancia);
            } else if(aglomerado2==null && aglomerado1!=null){
                aglomerado1.add(pontosDistancia.getPonto2());
                resultado.add(pontosDistancia);
            } else if(aglomerado1!=null && aglomerado2!=null){
                if(aglomerado1!=aglomerado2) {
                    aglomerado1.addAll(aglomerado2);
                    aglomerados.remove(aglomerado2);
                    resultado.add(pontosDistancia);
                }
                //Se estiverem no mesmo aglomerado, não adiciona à solução
            } else if(aglomerado1 == null && aglomerado2 == null){
                HashSet newSet = new HashSet();
                newSet.add(pontosDistancia.getPonto1());
                newSet.add(pontosDistancia.getPonto2());
                aglomerados.add(newSet);
                resultado.add(pontosDistancia);
            }
        }
        printTodosRamos();
    }

    private HashSet procuraEmAglomerado(String ponto) {
        HashSet toReturn = null;
        for(int n=0;n<aglomerados.size();n++){
            if(aglomerados.get(n).contains(ponto)){
                toReturn = aglomerados.get(n);
                break;
            }
        }
        return toReturn;
    }

    private void printTodosRamos(){
        for(int n=0;n<resultado.size();n++) {
            System.out.println(resultado.get(n).getPontosDistanciaDescription());
        }
    }
}
